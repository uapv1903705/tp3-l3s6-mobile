package fr.uavignon.shuet.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import fr.uavignon.shuet.tp3.data.SportDbHelper;
import fr.uavignon.shuet.tp3.data.Team;

public class MainActivity extends AppCompatActivity
{

    Cursor cursor;
    SportDbHelper sportDbHelper;
    ListView listView;

    @Override
    protected void onResume()
    {
        super.onResume();
        updateList();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "L'ajout d'equipe n'est pas implémenté", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        sportDbHelper = new SportDbHelper(this);

        updateList();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                goToTeamActivity(position);
            }
        });
        registerForContextMenu(listView);

    }

    public void goToTeamActivity(int position)
    {
        Intent appInfo = new Intent(MainActivity.this, TeamActivity.class);
        Parcelable teamParcelable = null;
        if(position < 0)    // NEW TEAM
        {
            teamParcelable = new Team("");
        }
        else                // MODIFY TEAM
        {
            cursor.moveToPosition(position);
            teamParcelable = sportDbHelper.cursorToTeam(cursor);
        }
        appInfo.putExtra(Team.TAG, teamParcelable);

        startActivity(appInfo);
    }


    public void updateList()
    {
        cursor = sportDbHelper.fetchAllTeams();
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                R.layout.teamlist,
                cursor,
                new String[] {sportDbHelper.COLUMN_TEAM_NAME, sportDbHelper.COLUMN_LEAGUE_NAME},
                new int[] {R.id.name, R.id.league},
                0
        );

        listView =  (ListView) findViewById(R.id.teamView);
        listView.setAdapter(adapter);
    }

}
