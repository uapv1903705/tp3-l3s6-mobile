package fr.uavignon.shuet.tp3;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import fr.uavignon.shuet.tp3.data.Match;
import fr.uavignon.shuet.tp3.data.SportDbHelper;
import fr.uavignon.shuet.tp3.data.Team;
import fr.uavignon.shuet.tp3.webservice.JSONResponseHandlerTeam;
import fr.uavignon.shuet.tp3.webservice.WebServiceUrl;

public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;
    private Button but;

    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;

    private SportDbHelper sportDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = (Team) getIntent().getParcelableExtra(Team.TAG);

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageBadge = (ImageView) findViewById(R.id.imageView);

        updateView();

        sportDbHelper = new SportDbHelper(this);

        but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                try
                {
                    but.setEnabled(false);
                    new AsyncTask<Void, Void, Team>()
                    {
                        @Override
                        protected Team doInBackground(Void... voids)
                        {
                            Team updatedTeam = new Team("");
                            Match updateMatch = new Match();

                            try
                            {
                                JSONResponseHandlerTeam JSONTeam = new JSONResponseHandlerTeam(updatedTeam);
                                HttpURLConnection urlConnection = null;

                                URL url = WebServiceUrl.buildSearchTeam(textTeamName.getText().toString());
                                urlConnection = (HttpURLConnection) url.openConnection();
                                InputStream in = urlConnection.getInputStream();
                                JSONTeam.readJsonStream(in);
                            }
                            catch (Exception e) { Log.e(TAG, "doInBackground: " + e ); }

                            updatedTeam.setId(team.getId());
                            return updatedTeam;
                        }

                        @Override
                        protected void onPostExecute(Team updatedTeam)
                        {
                            but.setEnabled(true);
                            team = updatedTeam;
                            sportDbHelper.updateTeam(updatedTeam);

                            updateView();
                        }
                    }.execute();
                }
                catch(Exception e)
                {
                    but.setEnabled(true);
                }
            }
        });

    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }

    private void updateView()
    {
        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
//        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());
    }

}
